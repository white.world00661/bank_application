package com.bank.library;

import java.math.*;

public class Account implements IAccount {

    private static int counter = 0;

    protected int _id;
    public final int getId() { return _id; }

    protected BigDecimal _sum;
    public final BigDecimal getSum() { return _sum; }

    protected int _percentage;
    public final int getPercentage() { return _percentage; }

    protected int _days = 0; // время с момента открытия счета

    public Account(BigDecimal sum, int percentage) {
        _sum = sum;
        _percentage = percentage;
        _id = ++counter;
    }

    public void Put(BigDecimal sum) {
        _sum = _sum.add(sum);
        System.out.println("На счет поступило " + sum);
    }

    public BigDecimal Withdraw(BigDecimal sum) {
        BigDecimal result = new BigDecimal(0);
        if (sum.compareTo(_sum) <= 0) {
            _sum = _sum.subtract(sum);
            result = sum;
            System.out.println("Сумма " + sum + " снята со счета " + _id);
        } else {
            System.out.println("Недостаточно денег на счете " + _id);
        }
        return result;
    }

    public void Transfer(BigDecimal sum, Account account) {
        if (sum.compareTo(_sum) <= 0) {
            _sum = _sum.subtract(sum);
            account._sum = account._sum.add(sum);
            System.out.println("Со счета id = " + this._id + " переведены средства на счет id = " + account._id + " в сумме: " + sum);
        } else {
            System.out.println("Недостаточно денег на счете " + this._id);
        }
    }

    public void CheckBalance() {
        System.out.println("На счету id = " + this._id + " денег: " + this._sum);
    }

    protected void Open() {
        System.out.println("Открыт новый депозитный счет!Id счета: " + this._id);
    }

    protected void Close() {
        System.out.println("Счет " + this._id + " закрыт.  Итоговая сумма: " + getSum());
    }

    protected final void IncrementDays() {
        _days++;
    }

    protected void Calculate() {
        BigDecimal increment = _sum.multiply(new BigDecimal( (double)_percentage / 100));
        _sum = _sum.add(increment);
        System.out.println("Начислены проценты на счет id = " + _id + " в размере: " + increment.setScale(2, BigDecimal.ROUND_DOWN));
    }

}
