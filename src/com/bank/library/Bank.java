package com.bank.library;

import java.math.*;

public class Bank {
    private Account[] accounts = null;

    private String Name;
    public final String getName() { return Name; }
    private void setName(String value) { Name = value; }

    public Bank(String name) {
        this.setName(name);
    }

    // Открытие счета
    public final void Open(AccountType accountType, BigDecimal sum) {
        Account newAccount = null;

        switch (accountType) {
            case Ordinary:
                newAccount = new DemandAccount(sum, 1);
                break;
            case Deposit:
                newAccount = new DepositAccount(sum, 40);
                break;
        }

        if (newAccount == null) {
            throw new RuntimeException("Ошибка создания счета");
        }

        if (accounts == null) {
            accounts = new Account[] {newAccount};
        } else {
            Account[] tempAccounts = new Account[accounts.length + 1];
            for (int i = 0; i < accounts.length; i++) {
                tempAccounts[i] = accounts[i];
            }
            tempAccounts[tempAccounts.length - 1] = newAccount;
            accounts = tempAccounts;
        }

        newAccount.Open();
    }

    // закрытие счета
    public final void Close(int id) {
        int index;
        tangible.OutObject<Integer> tempOut_index = new tangible.OutObject<Integer>();
        Account account = FindAccount(id, tempOut_index);
        index = tempOut_index.argValue;
        if (account == null) {
            throw new RuntimeException("Счет не найден");
        }

        account.Close();

        if (accounts.length <= 1) {
            accounts = null;
        } else {
            // уменьшаем массив счетов, удаляя из него закрытый счет
            Account[] tempAccounts = new Account[accounts.length - 1];
            for (int i = 0, j = 0; i < accounts.length; i++) {
                if (i != index) {
                    tempAccounts[j++] = accounts[i];
                }
            }
            accounts = tempAccounts;
        }
    }

    //добавление средств на счет
    public final void Put(BigDecimal sum, int id) {
        Account account = FindAccount(id);
        if (account == null) {
            throw new RuntimeException("Счет не найден");
        }
        account.Put(sum);
    }

    // вывод средств
    public final void Withdraw(BigDecimal sum, int id) {
        Account account = FindAccount(id);
        if (account == null) {
            throw new RuntimeException("Счет не найден");
        }
        account.Withdraw(sum);
    }

    // перевод средств
    public void Transfer(BigDecimal sum, int idOut, int idIn) {
        if (idOut == idIn) {
            throw new RuntimeException("Нельзя перевести самому себе");
        }

        Account accountOut = FindAccount(idOut);
        if (accountOut == null) {
            throw new RuntimeException("Счет отправителя не найден");
        }

        Account accountIn = FindAccount(idIn);
        if (accountIn == null) {
            throw new RuntimeException("Счет получетеля не найден");
        }

        accountOut.Transfer(sum, accountIn);
    }

    // Получение баланса
    public void CheckBalance(int id) {
        Account account = FindAccount(id);
        if (account == null) {
            throw new RuntimeException("Счет не найден");
        }
        account.CheckBalance();
    }

    // начисление процентов по счетам
    public final void CalculatePercentage(int days) {
        for (int day = 0; day < days; day++) {
            if (accounts == null) {
                return;
            }
            for (int i = 0; i < accounts.length; i++) {
                Account account = accounts[i];
                account.IncrementDays();
                account.Calculate();
            }
        }
    }

    // поиск счета по id
    public final Account FindAccount(int id) {
        if(accounts == null) {
            return null;
        }
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].getId() == id) {
                return accounts[i];
            }
        }
        return null;
    }

    // перегруженная версия поиска счета
    public final Account FindAccount(int id, tangible.OutObject<Integer> index) {
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].getId() == id) {
                index.argValue = i;
                return accounts[i];
            }
        }
        index.argValue = -1;
        return null;
    }

}
