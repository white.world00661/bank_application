package com.bank.library;

import java.math.*;

public interface IAccount {

    void Put(BigDecimal sum);

    BigDecimal Withdraw(BigDecimal sum);

}
