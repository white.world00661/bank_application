package com.bank.library;

public enum AccountType {
    Ordinary,
    Deposit;
}
