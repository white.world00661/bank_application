package com.bank.library;

import java.math.*;

public class DepositAccount extends Account {
    public DepositAccount(BigDecimal sum, int percentage) {
        super(sum, percentage);
    }

    @Override
    protected void Open() {
        System.out.println("Открыт новый депозитный счет! Id счета: " + this._id);
    }

    @Override
    public void Put(BigDecimal sum) {
        if (_days % 30 == 0) {
            System.out.println("На счет можно положить только после 30-ти дневного периода");
        } else {
            super.Put(sum);
        }
    }

    @Override
    public BigDecimal Withdraw(BigDecimal sum) {
        if (_days % 30 == 0) {
            System.out.println("Вывести средства можно только после 30-ти дневного периода");
        } else {
            return super.Withdraw(sum);
        }
        return new BigDecimal(0);
    }

    @Override public void Transfer(BigDecimal sum, Account account) {
        if (_days % 30 == 0) {
            System.out.println("Переводы со счета можно производить только после 30-ти дневного периода");
        } else {
            super.Transfer(sum, account);
        }
    }

    @Override
    protected void Calculate() {
        if (_days % 30 == 0) {
            super.Calculate();
        }
    }
}
