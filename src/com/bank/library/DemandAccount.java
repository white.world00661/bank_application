package com.bank.library;

import java.math.*;

public class DemandAccount extends Account {
    public DemandAccount(BigDecimal sum, int percentage) {
        super(sum, percentage);
    }

    @Override
    protected void Open() {
        System.out.println("Открыт новый счет до востребования! Id счета: " + this._id);
    }
}
