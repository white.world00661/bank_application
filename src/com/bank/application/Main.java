package com.bank.application;

import com.bank.library.*;

import java.util.*;
import java.math.*;

public class Main {

    public static void main(String[] args) {
        Bank bank = new Bank("Тинькоф");

        boolean alive = true;
        while (alive) {
            System.out.print(ConsoleColors.GREEN_BOLD_BRIGHT);
            System.out.println("1. Открыть счет \t 2. Вывести средства  \t 3. Добавить на счет");
            System.out.println("4. Закрыть счет \t 5. Перевести средства \t 6. Пропустить день");
            System.out.println("7. Узнать баланас \t 8. Выйти из программы");
            System.out.println("Введите номер пункта:");
            System.out.print(ConsoleColors.RESET);

            try {
                int command = Integer.parseInt(new Scanner(System.in).nextLine());
                switch (command) {
                    case 1:
                        OpenAccount(bank);
                        break;
                    case 2:
                        Withdraw(bank);
                        break;
                    case 3:
                        Put(bank);
                        break;
                    case 4:
                        CloseAccount(bank);
                        break;
                    case 5:
                        MoneyTransfer(bank);
                        break;
                    case 6:
                        MissedDay(bank);
                        break;
                    case 7:
                        CheckBalance(bank);
                        break;
                    case 8:
                        alive = false;
                        break;
                }
            } catch (RuntimeException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private static void OpenAccount(Bank bank) {
        System.out.println("Укажите сумму для создания счета:");
        BigDecimal sum = new BigDecimal(new Scanner(System.in).nextLine());

        System.out.println("Выберите тип счета: 1. До востребования 2. Депозит");
        int type = Integer.parseInt(new Scanner(System.in).nextLine());

        AccountType accountType;

        if (type == 2) {
            accountType = AccountType.Deposit;
        } else {
            accountType = AccountType.Ordinary;
        }

        bank.Open(accountType, sum);
    }

    private static void Withdraw(Bank bank) {
        System.out.println("Укажите сумму для вывода со счета:");
        BigDecimal sum = new BigDecimal(new Scanner(System.in).nextLine());

        System.out.println("Введите id счета:");
        int id = Integer.parseInt(new Scanner(System.in).nextLine());

        bank.Withdraw(sum, id);
    }

    private static void Put(Bank bank) {
        System.out.println("Укажите сумму, чтобы положить на счет:");
        BigDecimal sum = new BigDecimal(new Scanner(System.in).nextLine());

        System.out.println("Введите Id счета:");
        int id = Integer.parseInt(new Scanner(System.in).nextLine());

        bank.Put(sum, id);
    }

    private static void MoneyTransfer(Bank bank) {
        System.out.println("Укажите сумму перевода:");
        BigDecimal sum = new BigDecimal(new Scanner(System.in).nextLine());

        System.out.println("Введите Id счета с которого будет производиться перевод:");
        int idOut = Integer.parseInt(new Scanner(System.in).nextLine());

        System.out.println("Введите Id счета на который будут переведены средства:");
        int idIn = Integer.parseInt(new Scanner(System.in).nextLine());

        bank.Transfer(sum, idOut, idIn);
    }

    private static void MissedDay(Bank bank) {
        System.out.println("Введите сколько дней нужно пропустить:");
        int days = Integer.parseInt(new Scanner(System.in).nextLine());

        bank.CalculatePercentage(days);
    }

    private static void CheckBalance(Bank bank) {
        System.out.println("Введите Id счета:");
        int id = Integer.parseInt(new Scanner(System.in).nextLine());

        bank.CheckBalance(id);
    }

    private static void CloseAccount(Bank bank) {
        System.out.println("Введите id счета, который надо закрыть:");
        int id = Integer.parseInt(new Scanner(System.in).nextLine());

        bank.Close(id);
    }

}
